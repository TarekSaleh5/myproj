import string_finder


def test_first():
    answer = string_finder.is_string_exist("Right-click in the box below to see one called 'the-internet")
    assert answer == True


def test_second():
    answer = string_finder.is_string_exist("Alibaba")
    assert answer == True
